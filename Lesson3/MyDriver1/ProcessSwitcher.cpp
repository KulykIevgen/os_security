#include "ProcessSwitcher.h"

NTSTATUS ProcessSwitcher::RemoteExecute(ULONG ProcessId, RemoteProcessExec callback)
{
	PEPROCESS eprocess = nullptr;
	KAPC_STATE apcState;

	NTSTATUS status = PsLookupProcessByProcessId(reinterpret_cast<HANDLE> (ProcessId), &eprocess);
	DbgPrint("Status is %08X\n", status);
	if (NT_SUCCESS(status))
	{
		DbgPrint("PID before switch = %08X\n", PsGetCurrentProcessId());
		KeStackAttachProcess(eprocess, &apcState);
		ULONG result = callback();
		KeUnstackDetachProcess(&apcState);
		ObDereferenceObject(eprocess);
		DbgPrint("PID after switch = %08X\n", result);
	}

	return status;
}

void RemoteExecuteCallback(ULONG ProcessId)
{
	ProcessSwitcher::RemoteExecute(ProcessId, ProcessSwitcher::ReadCurrentPid);
}
