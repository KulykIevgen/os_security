#pragma once
#include <ntifs.h>

typedef ULONG (*RemoteProcessExec)();

class ProcessSwitcher
{
public:
	static NTSTATUS RemoteExecute(ULONG ProcessId, RemoteProcessExec callback);

	ProcessSwitcher() = delete;
	~ProcessSwitcher() = delete;
	ProcessSwitcher(const ProcessSwitcher&) = delete;
	ProcessSwitcher(ProcessSwitcher&&) = delete;
	ProcessSwitcher& operator=(const ProcessSwitcher&) = delete;
	ProcessSwitcher& operator=(ProcessSwitcher&&) = delete;

	static ULONG ReadCurrentPid()
	{
		return reinterpret_cast<ULONG> ( PsGetCurrentProcessId() );
	}
};

extern "C" void RemoteExecuteCallback(ULONG ProcessId);