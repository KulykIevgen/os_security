#pragma warning( disable : 4100)
#include <wdm.h>

extern void RemoteExecuteCallback(ULONG ProcessId);

#define SYMLINK_NAME L"\\DosDevices\\MemReaderLink"
#define DEVICE_NAME L"\\Device\\MemReader"

VOID OnUnload(IN PDRIVER_OBJECT DriverObject)
{
	UNICODE_STRING SymLink = RTL_CONSTANT_STRING(SYMLINK_NAME);
	IoDeleteSymbolicLink(&SymLink);
	IoDeleteDevice(DriverObject->DeviceObject);
	DbgPrint("OnUnload called\n");	
}

NTSTATUS Default_Handler(PDEVICE_OBJECT DeviceObject, PIRP Irp)
{
	DbgPrint("Open/Close handle\n");
	Irp->IoStatus.Information = 0;
	Irp->IoStatus.Status = STATUS_SUCCESS;
	IoCompleteRequest(Irp, IO_NO_INCREMENT);
	return STATUS_SUCCESS;
}

NTSTATUS Write_Handler(PDEVICE_OBJECT DeviceObject, PIRP Irp)
{	
	PIO_STACK_LOCATION IoStackIrp = IoGetCurrentIrpStackLocation(Irp);
	if (IoStackIrp)
	{
		ULONG OutputLength = IoStackIrp->Parameters.Write.Length;
		DbgPrint("Current output is %u\n", OutputLength);
		DbgPrint("SystemBuffer is %p\n", Irp->AssociatedIrp.SystemBuffer);		
	
		if (Irp->AssociatedIrp.SystemBuffer)
		{
			ULONG ProcessId = *(PULONG)Irp->AssociatedIrp.SystemBuffer;
			DbgPrint("Trying to inject inside process with id %u\n", ProcessId);
			RemoteExecuteCallback(ProcessId);
			Irp->IoStatus.Information = OutputLength;
			Irp->IoStatus.Status = STATUS_SUCCESS;
		}
		else
		{
			Irp->IoStatus.Information = 0;
			Irp->IoStatus.Status = STATUS_BAD_DATA;
		}
		
	}
	IoCompleteRequest(Irp, IO_NO_INCREMENT);
	return STATUS_SUCCESS;
}

NTSTATUS DriverEntry(IN PDRIVER_OBJECT theDriverObject,
	IN PUNICODE_STRING theRegistryPath)
{
	DbgPrint("I�m loaded!\n");

	PDEVICE_OBJECT TheDevice = NULL;
	theDriverObject->DriverUnload = OnUnload;
	UNICODE_STRING DeviceName = RTL_CONSTANT_STRING(DEVICE_NAME);
	NTSTATUS status = IoCreateDevice(theDriverObject, 0, &DeviceName, FILE_DEVICE_UNKNOWN, 
		FILE_DEVICE_SECURE_OPEN, FALSE, &TheDevice);

	if (NT_SUCCESS(status))
	{
		for (unsigned int i = 0; i < IRP_MJ_MAXIMUM_FUNCTION; i++)
		{
			theDriverObject->MajorFunction[IRP_MJ_CREATE] =
				theDriverObject->MajorFunction[IRP_MJ_CLOSE] = Default_Handler;
			theDriverObject->MajorFunction[IRP_MJ_WRITE] = Write_Handler;
		}

		TheDevice->Flags |= DO_BUFFERED_IO;		

		UNICODE_STRING SymLink = RTL_CONSTANT_STRING(SYMLINK_NAME);
		status = IoCreateSymbolicLink(&SymLink, &DeviceName);
	}

	return status;
}
