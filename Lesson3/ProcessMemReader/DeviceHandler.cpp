#include "DeviceHandler.h"
#include "ErrorInfo.h"
#include <iostream>

DeviceHandler::DeviceHandler(const std::string& DeviceName)
{
	devObject = CreateFileA(DeviceName.c_str(), GENERIC_READ | GENERIC_WRITE, 0, nullptr, OPEN_EXISTING, 0, 0);
	if (devObject == INVALID_HANDLE_VALUE)
	{
		throw ErrorInfo::MakeLastError();
	}
}

DeviceHandler::~DeviceHandler()
{
	CloseHandle(devObject);
}

void DeviceHandler::Write(DWORD ProcessId) const
{
	DWORD LocalCopy = ProcessId;
	DWORD result = 0;

	if (!WriteFile(devObject, &LocalCopy, sizeof(LocalCopy), &result, nullptr))
	{
		throw ErrorInfo::MakeLastError();
	}
}
