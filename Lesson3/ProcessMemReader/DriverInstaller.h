#pragma once
#include <string>
#include <Windows.h>
#include "SCManager.h"

class DriverInstaller
{
public:	
	explicit DriverInstaller(const std::string& DriverPath);
	~DriverInstaller();
	void Start();
	void Stop();
	void Delete();

	DriverInstaller() = delete;
	DriverInstaller(const DriverInstaller&) = default;
	DriverInstaller(DriverInstaller&&) = default;
	DriverInstaller& operator=(const DriverInstaller&) = default;
	DriverInstaller& operator=(DriverInstaller&&) = default;

private:
	SCManager scmanager;
	SC_HANDLE service;
};

