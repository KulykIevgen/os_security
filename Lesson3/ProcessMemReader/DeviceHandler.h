#pragma once
#include <string>
#include <Windows.h>

class DeviceHandler
{
public:
	explicit DeviceHandler(const std::string& DeviceName);
	~DeviceHandler();
	void Write(DWORD ProcessId) const;

	DeviceHandler() = delete;
	DeviceHandler(const DeviceHandler&) = default;
	DeviceHandler(DeviceHandler&&) = default;
	DeviceHandler& operator=(const DeviceHandler&) = default;
	DeviceHandler& operator=(DeviceHandler&&) = default;

private:
	HANDLE devObject;
};

