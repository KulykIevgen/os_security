#include "DriverInstaller.h"
#include "ErrorInfo.h"

DriverInstaller::DriverInstaller(const std::string& DriverPath)
{
	const char* shortName = "Lesson3";
	const auto ACCESS_TYPE = (SERVICE_START | DELETE | SERVICE_STOP);

	service = CreateServiceA(scmanager.GetHandle(), shortName, shortName, ACCESS_TYPE,
		SERVICE_KERNEL_DRIVER, SERVICE_DEMAND_START, SERVICE_ERROR_IGNORE, DriverPath.c_str(),
		nullptr, nullptr, nullptr, nullptr, nullptr);

	if (!service)
	{
		throw ErrorInfo::MakeLastError();
	}
}

DriverInstaller::~DriverInstaller()
{
	CloseServiceHandle(service);
}

void DriverInstaller::Start()
{
	if (!StartService(service, 0, nullptr))
	{
		throw ErrorInfo::MakeLastError();
	}
}

void DriverInstaller::Stop()
{
	SERVICE_STATUS status = { 0 };

	if (!ControlService(service, SERVICE_CONTROL_STOP, &status))
	{
		throw ErrorInfo::MakeErrorFromCode(status.dwWin32ExitCode);
	}
}

void DriverInstaller::Delete()
{
	if (!DeleteService(service))
	{
		throw ErrorInfo::MakeLastError();
	}
}
