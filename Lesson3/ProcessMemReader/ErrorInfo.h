#pragma once
#include <Windows.h>
#include <stdexcept>
#include <string>

class ErrorInfo
{
public:
	static std::logic_error MakeErrorFromCode(DWORD ErrorCode);
	static std::logic_error MakeLastError();

	ErrorInfo() = delete;
	~ErrorInfo() = delete;
	ErrorInfo& operator=(const ErrorInfo&) = delete;
	ErrorInfo& operator=(ErrorInfo&&) = delete;
	ErrorInfo(const ErrorInfo&) = delete;
	ErrorInfo(ErrorInfo&&) = delete;
};
