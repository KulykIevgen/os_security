#include "DriverWorker.h"
#include <iostream>

DriverWorker::DriverWorker(const std::string& DriverPath, const std::string& DevName)
{
	driverInstaller.reset(new DriverInstaller(DriverPath));
	driverInstaller->Start();
	deviceHandler.reset(new DeviceHandler(DevName));
}

DriverWorker::~DriverWorker()
{
	try {
		driverInstaller->Stop();
		driverInstaller->Delete();
	}
	catch (const std::exception& error)
	{
		std::cout << error.what() << std::endl;
	}
}

BYTE DriverWorker::Write(DWORD ProcessId) const
{
	deviceHandler->Write(ProcessId);
	return 0;
}
