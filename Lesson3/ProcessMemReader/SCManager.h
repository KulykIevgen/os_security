#pragma once
#include <Windows.h>

class SCManager
{
public:
	SCManager();
	~SCManager();
	SC_HANDLE GetHandle() const;

	SCManager(const SCManager&) = default;
	SCManager(SCManager&&) = default;
	SCManager& operator=(const SCManager&) = default;
	SCManager& operator=(SCManager&&) = default;
private:
	SC_HANDLE manager;
};
