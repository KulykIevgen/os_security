#pragma once
#include "DeviceHandler.h"
#include "DriverInstaller.h"
#include <string>
#include <memory>

class DriverWorker
{
public:
	explicit DriverWorker(const std::string& DriverPath,const std::string& DevName);
	~DriverWorker();
	BYTE Write(DWORD ProcessId) const;

	DriverWorker() = delete;
	DriverWorker(const DriverWorker&) = delete;
	DriverWorker(DriverWorker&&) = delete;
	DriverWorker& operator=(const DriverWorker&) = delete;
	DriverWorker& operator=(DriverWorker&&) = delete;

private:
	std::unique_ptr<DriverInstaller> driverInstaller;
	std::unique_ptr<DeviceHandler> deviceHandler;
};

