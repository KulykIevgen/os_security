#include "SCManager.h"
#include "ErrorInfo.h"

SCManager::SCManager()
{
	manager = OpenSCManager(nullptr, nullptr, SC_MANAGER_ALL_ACCESS);
	if (!manager)
	{
		throw ErrorInfo::MakeLastError();
	}
}

SCManager::~SCManager()
{
	CloseServiceHandle(manager);
}

SC_HANDLE SCManager::GetHandle() const
{
	return manager;
}