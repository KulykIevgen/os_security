#include "DriverInstaller.h"
#include "DeviceHandler.h"
#include "DriverWorker.h"

#include <iostream>

int main()
{
	try {
		char currentDir[MAX_PATH] = { 0 };
		auto realSize = GetCurrentDirectoryA(sizeof(currentDir) - sizeof(currentDir[0]), currentDir);
		std::string path(currentDir, realSize);
		std::string fullPath = path + "\\MemReader.sys";
		std::cout << fullPath << std::endl;

		DriverWorker driverWorker(fullPath, "\\\\.\\MemReaderLink");
		std::cout << std::hex << driverWorker.Write(4) << std::endl;
	}
	catch (const std::exception& error)
	{
		std::cout << error.what() << std::endl;
	}
    return 0;
}

